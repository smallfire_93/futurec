
function getSettings(callback) {
  getJSON("http://famtechvn.net:9991/settings", callback);
}

function login(username, pwd, code, callback) {
  postJSON("http://famtechvn.net:9991/user/login", {
    "username": username,
    "password": pwd,
    "totp": code
  }, callback);
}

function register(username, email, pwd, ref, callback) {
  postJSON("http://famtechvn.net:9991/user/register", {
    "referrer": ref,
    "username": username,
    "email": email,
    "password": pwd
  }, callback);
}

function presale(name, email, country, link, number, callback) {
  postJSON("http://famtechvn.net:9991/presale", {
    "name": name,
    "email": email,
    "country": country,
    "link": link,
    "number": number
  }, callback);
}

function subscribed(email, callback) {
  postJSON("http://famtechvn.net:9991/subscribe", { "email": email }, callback);
}

function sendFeedback(name,email,phone,country,message, callback) {
  postJSON("http://famtechvn.net:9991/contact", {
    "name": name,
    "email": email,
    "phone": phone,
    "country": country,
    "message": message
  }, callback);
}

function getNews(callback) {
  getJSON("http://famtechvn.net:9991/news", callback);
}

function getLendingPrograms(callback) {
  getJSON("http://famtechvn.net:9991/lending/packages", callback);
}

function getJSON(url, callback) {
  console.log("request :"+url);
  $.getJSON(url, {}, function (data, status) {
    if (status === 'success') {
      if(data.success){
        callback(data.data, null);
      }else{
        callback(null, data.error);
      }
    } else {
      callback(null, data);
    }
    console.log("response:"+JSON.stringify(data));
  });
}

function postJSON(url, json, callback) {
  console.log("request :"+url+" data: "+JSON.stringify(json));
  $.post(url, json, function (data, status) {
    if (status === 'success') {
      if(data.success){
        callback(data.data, null);
      }else{
        callback(null, data.error);
      }
    } else {
      callback(null, data);
    }
    console.log("response:"+JSON.stringify(data));
  });
}