$(document).ready(function () {

    $("[ref='login_form']").submit(function (event) {
        event.preventDefault();
        var btnLogin = $("[ref='btnLogin']");
        var email = $("[ref='login_email']").val();
        var pwd = $("[ref='login_pwd']").val();
        var code = $("[ref='login_twofa']").val();
        var captchaResponse;
		if(grecaptcha){
			captchaResponse = grecaptcha.getResponse(loginCaptcha);
		}
		if(captchaResponse){
			grecaptcha.reset(loginCaptcha);
			btnLogin.button('loading');
            login(email, pwd, code, function (data, err) {
                btnLogin.button('reset');
                if (err) {
					toastr.error(err, 'OOP!')
				} else {
					toastr.success('Login success !', 'Success')
				}
            })
		}else{
			toastr.error("Please verify you are not a robot", 'OOP!')
		}
    });

    $("[ref='register_form']").submit(function (event) {
        event.preventDefault();
        var btnRegister = $("[ref='btnRegister']");
        var ref = $("[ref='register_ref']").val();
        var username = $("[ref='register_username']").val();
        var email = $("[ref='register_email']").val();
        var pwd = $("[ref='register_pwd']").val();
        var repwd = $("[ref='register_repwd']").val();
        var captchaResponse;
		if(grecaptcha){
			captchaResponse = grecaptcha.getResponse(registerCaptcha);
		}
		if(captchaResponse){
			grecaptcha.reset(registerCaptcha);
			if(pwd != repwd){
                toastr.error("retype password not match.", 'OOP!');
                return;
            }
            btnRegister.button('loading');
            register(username, email, pwd,ref, function (data, err) {
                btnRegister.button('reset');
                if (err) {
					toastr.error(err, 'OOP!')
				} else {
					toastr.success("A Verification link has been sent to "+email+". Please Check the indicated email address ", 'Success')
				}
            })
		}else{
			toastr.error("Please verify you are not a robot", 'OOP!')
        }
    });

});