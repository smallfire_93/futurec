$(document).ready(function() {

	var b = true;

	getSettings(function(data, err) {
		if(data) {
			bindPreSaleData(data.presale);
			startCountDownTimer(new Date());
			bindICOData(data.ico);
		}
	});

	getNews(function(data, err) {
		if(data) {
			var html     = '';
			var carousel = $("[ref='newsList']");
			carousel.owlCarousel({
				items     : 3,
				loop      : true,
				// autoplay  : true,
				margin    : 20,
				// autoplayTimeout   : 2000,
				// autoplayHoverPause: true,
				// nav  : true,
				center    : false,
				// dots              : true
				//
				responsive: {
					935: {
						items: 3
					},
					768: {
						items: 2
					},
					480: {
						items: 1
					},
					360: {
						items: 1
					},
					0  : {
						items: 1
					}
				}
			});
			for(i = 0; i < data.length; i++) {
				var news = data[i];
				html     = "<div class='newsItem'>" +
					"<a href='javascript:;' class='imgBox'><img src='" + news.image + "' width='380' height='246' alt=''/></a>" +
					"<h3><a href='javascript:;'>" + news.title + "</a></h3>" +
					"<p>" + news.content + "</p>" +
					"</div>";
				carousel.trigger('add.owl.carousel', [html]).trigger('refresh.owl.carousel');
			}
		}
	});

	getLendingPrograms(function(data, err) {
		if(data) {
			var html     = '';
			var carousel = $("[ref='lendingList']");
			carousel.owlCarousel({

				items     : 5,
				loop      : true,
				autoplay  : true,
				margin    : 20,
				// autoplayTimeout   : 2000,
				// autoplayHoverPause: true,
				// nav  : true,
				center    : true,
				// dots              : true
				//
				responsive: {
					935: {
						items: 5
					},
					768: {
						items: 2
					},
					480: {
						items: 1
					},
					360: {
						items: 1
					},
					0  : {
						items: 1
					}
				}
			});
			for(i = 0; i < data.length; i++) {
				var package = data[i];
				html        = "<div class='lendingItem col-sm-2 col-xs-12'>" +
					"<p class='dolar'>" + package.min + "$ - " + package.max + "$</p>" +
					"<p class='profit'>" + package.type + "Profit<span>+" + package.profit + "%</span></p>" +
					"<p class='days'>" + package.period + " Days</p>" +
					"<a href='#backtotop' class='btnLend'>Lend</a>" +
					"</div>";
				carousel.trigger('add.owl.carousel', [html]).trigger('refresh.owl.carousel');
			}
		}
	});

	$("[ref='btnBuy']").click(() = > {
		window.open("login.html");
})
	;

	$("[ref='sub_form']").submit(function(event) {
		event.preventDefault();
		var $this = $("[ref='btnSub']");
		var email = $("[ref='sub_email']").val();
		if(email) {
			// $("[ref='sub_status']").text("subscribe " + JSON.stringify({ "email": email }));
			$this.button('loading');
			subscribed(email, function(data, err) {
				$this.button('reset');
				if(err) {
					toastr.error(err, 'OOP!')
				} else {
					toastr.success('Your email send success !', 'Success')
				}
			})
		}
	});

	$("[ref='contact_form']").submit(function(event) {
		event.preventDefault();
		var $this   = $("[ref='btnSend']");
		var name    = $("[ref='contact_name']").val();
		var email   = $("[ref='contact_email']").val();
		var phone   = $("[ref='contact_phone']").val();
		var country = $("[ref='contact_country']").val();
		var message = $("[ref='contact_message']").val();
		var contactCaptchaResponse;
		if(grecaptcha) {
			contactCaptchaResponse = grecaptcha.getResponse(contactCaptcha);
		}
		if(contactCaptchaResponse) {
			grecaptcha.reset(contactCaptcha);
			$this.button('loading');
			sendFeedback(name, email, phone, country, message, function(data, err) {
				$this.button('reset');
				if(err) {
					toastr.error(err, 'OOP!')
				} else {
					toastr.success('Your feedback send success !', 'Success')
				}
			})
		} else {
			toastr.error("Please verify you are not a robot", 'OOP!')
		}
	});

	$("[ref='presale_form']").submit(function(event) {
		event.preventDefault();
		var $this   = $("[ref='btnPresaleRegister']");
		var name    = $("[ref='presale_name']").val();
		var email   = $("[ref='presale_email']").val();
		var country = $("[ref='presale_country']").val();
		var link    = $("[ref='presale_link']").val();
		var number  = $("[ref='presale_number']").val();
		var presaleCaptchaResponse;
		if(grecaptcha) {
			presaleCaptchaResponse = grecaptcha.getResponse(presaleCaptcha);
		}
		if(presaleCaptchaResponse) {
			grecaptcha.reset(presaleCaptcha);
			$this.button('loading');
			presale(name, email, country, link, number, function(data, err) {
				$this.button('reset');
				$("[ref='presale_modal']").modal('hide');
				if(err) {
					toastr.error(err, 'OOP!')
				} else {
					toastr.success('Register for Pre-Sale package send !', 'Success')
				}
			})
		} else {
			toastr.error("Please verify you are not a robot", 'OOP!')
		}
	});

	$('#content .stream .stream ul li a').click(function() {
		$('#content .stream .stream ul li a').removeClass('active');
		$(this).addClass('active');
		$('#content .stream .stream ul li div').stop().slideUp();
		$(this).next().stop().slideToggle();
	});

	$('#content .pricetable .priceTab li a').click(function() {
		$('#content .pricetable .priceTab li a').css('display', 'block');
		$(this).hide();
		if(b) {
			$('#content .pricetable .hideBox').css({'height': 'auto'});
			$('#content .pricetable .priceTab .hideBg').hide();
			b = false;
		}
		else {
			$('#content .pricetable .hideBox').css({'height': '262px'});
			$('#content .pricetable .priceTab .hideBg').show();
			b = true;
		}
	});

	function bindPreSaleData(data) {
		$('p.date').text(data.time_start);
		$('#tokensale .detail #total_supply').text(data.total_supply.toLocaleString('en') + " ROC");
		$('#tokensale .detail #crowdsale').text(data.crowdsale.toLocaleString('en') + " ROC");
		$('#tokensale .detail #accepted_currency').text(data.accepted_currency);
		$('#tokensale .detail #round_number').text(data.round_number);
		$('#tokensale .detail #round_price').text(data.round_price + "$");
	}

	function startCountDownTimer(time) {
		if(time) {
			$('#tokensale .time').countdown(time, function(event) {
				$('#tokensale .time #days').text(event.strftime('%D'));
				$('#tokensale .time #hours').text(event.strftime('%H'));
				$('#tokensale .time #minutes').text(event.strftime('%M'));
				$('#tokensale .time #seconds').text(event.strftime('%S'));
			});
		}
	}

	function bindICOData(ico) {
		$('#pricetable [ref="total_supply"]').text(ico.total_supply.toLocaleString('en'));
		$('#pricetable [ref="ico"]').text(ico.ico.toLocaleString('en'));
		$('#pricetable [ref="blocks"]').text(ico.blocks);
		$('#pricetable [ref="days_of_sale"]').text(ico.days_of_sale);
		$('#pricetable [ref="fund_raised"]').text(ico.fund_raised.toLocaleString('en') + "$");
		for(i = 0; i < ico.price_table.length; i++) {
			var block = ico.price_table[i];
			$("#pricebox").append(
				"<tr>" +
				"<td>" + block.date + "</td>" +
				"<td>" + block.time + "</td>" +
				"<td>" + block.token + "</td>" +
				"<td>" + block.price + "$</td>" +
				"<td><span class='load'><span></span></span> " + block.status + "</td>" +
				"</tr>"
			);
		}
	}
});