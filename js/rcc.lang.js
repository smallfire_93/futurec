$(document).ready(function(){
var langs = ['en', 'ko','ru','zh'];
var langCode = 'en';
var langJS = null;

  // Handle language change
  $("[lang='en']").click(function(){
      langCode = 'en';
      changeLanguage();
  });
  $("[lang='ko']").click(function(){
      langCode = 'ko';
      changeLanguage();
  });
  $("[lang='zh']").click(function(){
      langCode = 'zh';
      changeLanguage();
  });
  $("[lang='ru']").click(function(){
      langCode = 'ru';
      changeLanguage();
  });
  
  var translate = function (jsdata)
  {	
	  $("[tkey]").each (function (index)
	  {
      var strTr = jsdata [$(this).attr ('tkey')];
	    $(this).text(strTr);
	  });
  }

  function changeLanguage(){
    if (langs.indexOf(langCode)){
      $.getJSON('assets/lang/'+langCode+'.json', translate);
    }else{
      $.getJSON('assets/lang/en.json', translate);
    }
  }
  
  changeLanguage();
  
});