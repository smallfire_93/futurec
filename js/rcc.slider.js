// $(document).ready(function() {

$(".newsList").owlCarousel({

	items     : 3,
	loop      : true,
	autoplay  : true,
	margin    : 20,
	// autoplayTimeout   : 2000,
	// autoplayHoverPause: true,
	// nav  : true,
	center    : false,
	// dots              : true
	//
	responsive: {
		935: {
			items: 3
		},
		768: {
			items: 2
		},
		480: {
			items: 1
		},
		360: {
			items: 1
		},
		0  : {
			items: 1
		}
	}
});
$(".lendingList").owlCarousel({

	items     : 5,
	loop      : true,
	autoplay  : true,
	margin    : 20,
	// autoplayTimeout   : 2000,
	// autoplayHoverPause: true,
	// nav  : true,
	center    : true,
	// dots              : true
	//
	responsive: {
		935: {
			items: 5
		},
		768: {
			items: 2
		},
		480: {
			items: 1
		},
		360: {
			items: 1
		},
		0  : {
			items: 1
		}
	}
});

// });